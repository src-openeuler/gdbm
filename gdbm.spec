Name:       gdbm
Version:    1.23
Release:    2
Epoch:      1
Summary:    A library of database functions that work similar to the standard UNIX dbm
License:    GPLv3+
URL:        http://www.gnu.org/software/gdbm/
Source0:    http://ftp.gnu.org/gnu/gdbm/gdbm-%{version}.tar.gz

Patch0:     Fix-binary-dump-format-for-key-and-or-data-of-zero-s.patch
Patch1:     gdbm_dump-fix-command-line-error-detection.patch
Patch2:     Fix-location-tracking-in-gdbmtool.-Fix-the-recover-c.patch
Patch3:     Fix-coredump-in-gdbmtool-history-command.patch
Patch4:     Fix-semantics-of-gdbm_load-r.patch
Patch5:     Improve-handling-of-u-in-gdbm_load.patch
Patch6:     Fix-allocated-memory-not-released.patch
Patch7:     Restore-accidentally-removed-parameter-and-New-macro.patch
Patch8:     Minor-fix-in-the-compatibility-library.patch

BuildRequires:  gcc libtool gettext readline-devel bison flex texinfo

Provides:   %{name}-libs
Provides:   %{name}-libs%{?_isa}
Obsoletes:  %{name}-libs <= %{version}

%description
GNU dbm (or GDBM, for short) is a library of database functions that
use extensible hashing and work similar to the standard UNIX dbm.
These routines are provided to a programmer needing to create and
manipulate a hashed database.

%package            devel
Summary:            Header files and libraries for development
Requires:           %{name} = %{epoch}:%{version}-%{release}
Requires(post):     info
Requires(preun):    info
Obsoletes:          %{name}-libs <= %{version}

%description        devel
Gdbm-devel contains the header files and libraries for development.
You should install this package if you want to develop applications
base on gdbm database.

%package            help
Summary:            Documents for %{name}
Buildarch:          noarch
Requires:           man info

%description        help
Man pages and other related documents for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static --disable-largefile --disable-rpath --enable-libgdbm-compat
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%make_build

%install
%make_install

%find_lang %{name}

# create symlinks for compatibility
mkdir -p $RPM_BUILD_ROOT/%{_includedir}/gdbm
ln -sf ../gdbm.h $RPM_BUILD_ROOT/%{_includedir}/gdbm/gdbm.h
ln -sf ../ndbm.h $RPM_BUILD_ROOT/%{_includedir}/gdbm/ndbm.h
ln -sf ../dbm.h $RPM_BUILD_ROOT/%{_includedir}/gdbm/dbm.h

rm -f $RPM_BUILD_ROOT/%{_infodir}/dir

%check
export LD_LIBRARY_PATH=`pwd`/src/.libs/:`pwd`/compat/.libs/
make check

%ldconfig_scriptlets

%post devel
/sbin/install-info %{_infodir}/gdbm.info.gz %{_infodir}/dir \
      --entry="* gdbm: (gdbm).                   The GNU Database." || :

%preun devel
if [ $1 = 0 ]; then
    /sbin/install-info --delete %{_infodir}/gdbm.info.gz %{_infodir}/dir \
        --entry="* gdbm: (gdbm).                   The GNU Database." || :
fi

%files -f %{name}.lang
%doc NEWS README THANKS AUTHORS NOTE-WARNING
%license COPYING
%{_bindir}/gdbm*
%{_libdir}/libgdbm.so.6*
%{_libdir}/libgdbm_compat.so.4*
%exclude %{_libdir}/*.la

%files devel
%{_libdir}/libgdbm.so
%{_libdir}/libgdbm_compat.so
%{_includedir}/*
%{_infodir}/*.info*

%files help
%{_mandir}/man1/gdbm*
%{_mandir}/man3/*
%{_infodir}/*.info*

%changelog
* Thu Mar 16 2023 wangzhiqiang <wangzhiqiang95@huawei.com> - 1:1.23-2
- DESC:backport upstream patch

* Thu Dec 8 2022 wangzhiqiang <wangzhiqiang95@huawei.com> - 1:1.23-1
- DESC: update to version 1.23

* Tue Aug 30 2022 yangchenguang <yangchenguang@uniontech.com> - 1:1.22-10
- DESC: Fix allocated memory not released

* Tue Jul 5 2022 wangzhqiang <wangzhiqiang95@huawei.com> - 1:1.22-9
- DESC: Improve handling of -u in gdbm_load

* Mon Jul 4 2022 wangzhqiang <wangzhiqiang95@huawei.com> - 1:1.22-8
- DESC: Fix semantics of gdbm_load -r

* Tue Jun 28 2022 wangzhqiang <wangzhiqiang95@huawei.com> - 1:1.22-7
- DESC: Fix coredump in gdbmtool history command

* Thu Jun 23 2022 wangzhqiang <wangzhiqiang95@huawei.com> - 1:1.22-6
- DESC: fix location tracking in gdbmtool, fix the recover command 

* Wed Jun 22 2022 wangzhqiang <wangzhiqiang95@huawei.com> - 1:1.22-5
- DESC: gdbm_dump-fix-command-line-error-detection

* Fri Jun 17 2022 wangzhiqiang <wangzhiqiang95@huawei.com> - 1:1.22-4
- DESC: Fix binary dump format for key and or data of zero size

* Tue Mar 1 2022 yanglongkang <yanglongkang@h-partners.com> - 1.22-3
- add info into the devel package to resolve the installation error

* Thu Dec 09 2021 yangchenguang <yangchenguang@uniontech.com> - 1.22-2
- Fix gdbmtool parm import error

* Wed Nov 17 2021 Wenchao Hao <haowenchao@huawei.com> - 1.22-1
- Update to gdbm-1.22

* Fri Sep 10 2021 geruijun <geruijun@huawei.com> - 1.20-3
- DESC: fix stack overflow in print_usage

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.20-2
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Tue Jul 20 2021 yanglongkang <yanglongkang@huawei.com> - 1.20-1
- upgrade package to 1.20

* Sat Jul 18 2020 Wangjun <wangjun196@huawei.com> - 1.18.1-1
- upgrade package to 1.18.1 

* Sat Aug 31 2019 guiyao <guiyao@huawei.com> - 1.18-2
- Package init
